# Create your views here.
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from initial_project import settings
from .forms import *
from django.contrib.formtools.wizard.views import SessionWizardView
import logging
logging.basicConfig()
logr = logging.getLogger(__name__)
from django.db import transaction
from django.shortcuts import get_object_or_404


def homepage(request):

    context = {
        'project_name': settings.SUIT_CONFIG.get('ADMIN_NAME')
    }
    return render_to_response('index.html', context, RequestContext(request))


class MyAccountWizard(SessionWizardView):

    def get_template_names(self):
        template_dict = {
            '0': 'accounts.html',
            '1': 'packages.html',
            '2': 'contacts.html',
        }

        return template_dict[self.steps.current]

    def get_context_data(self, form, **kwargs):
        context = super(MyAccountWizard, self).get_context_data(form=form, **kwargs)

        context.update({
            'project_name': settings.SUIT_CONFIG.get('ADMIN_NAME'),
            'form': form,
        })
        return context

    def get_form(self, step=None, data=None, files=None):
        #Aqui vao as regras de autorizacao
        form = super(MyAccountWizard, self).get_form(step, data, files)
        return form

    @transaction.commit_on_success
    def done(self, form_list, **kwargs):

        # ACCOUNT ###############################
        account_form = form_list[0]
        account = account_form.save()

        # PACKAGES ##############################
        packages_formset = form_list[1]
        packages_instances = packages_formset.save(commit=False)
        for package in packages_instances:
            package.account_id = account.id
            package.save()
        packages_formset.save_m2m()

        # CONTACTS ##############################
        contact_formset = form_list[2]
        contact_instances = contact_formset.save(commit=False)
        for contact in contact_instances:
            contact.account_id = account.id
            contact.save()

        return HttpResponseRedirect(reverse('homepage'))


def account_create(request):

    instance_dict = {
        '0': None,
        '1': Packages.objects.none(),
        '2': Contact.objects.none()
    }

    return MyAccountWizard.as_view([AccountForm, PackageFormSet, ContactFormSet], instance_dict=instance_dict)(request)


def account_edit(request):
    account = Account.objects.get(name='azion_test')
    packages = Packages.objects.filter(account=account)
    contacts = Contact.objects.filter(account=account)

    instance_dict = {
        '0': account,
        '1': packages,
        '2': contacts
    }

    return MyAccountWizard.as_view([AccountForm, PackageFormSet, ContactFormSet], instance_dict=instance_dict)(request)


def package_edit(request, package_id):
    package = get_object_or_404(Packages, pk=package_id)

    PackagePricesInline = inlineformset_factory(Packages, PackagesPrices, extra=1, can_delete=True)

    if request.method == 'POST':
        formset = PackagePricesInline(request.POST, instance=package)

        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect(reverse('homepage'))
    else:
        formset = PackagePricesInline(instance=package)

    context = {
        'package': package,
        'formset': formset,
    }

    return render_to_response('packages_edit.html', context, RequestContext(request))


def package_create(request):

    PackagePricesInline = inlineformset_factory(Packages, PackagesPrices, extra=2, can_delete=True)

    if request.method == 'POST':
        package_form = PackageForm(request.POST)

        if package_form.is_valid():
            package = package_form.save(commit=False)
            formset = PackagePricesInline(request.POST, instance=package)

            if formset.is_valid():
                package.save()
                formset.save()
                return HttpResponseRedirect(reverse('homepage'))
        else:
            formset = PackagePricesInline(request.POST)
    else:
        package_form = PackageForm()
        formset = PackagePricesInline()

    context = {
        'package_form': package_form,
        'formset': formset,
    }

    return render_to_response('package_create.html', context, RequestContext(request))

