from django.db import models


class LocationLevel(models.Model):
    name = models.CharField(max_length=100)
    level = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class Location(models.Model):
    location_level = models.ForeignKey(LocationLevel)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class Currency(models.Model):
    name = models.CharField(max_length=100)
    symbol = models.CharField(max_length=5)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class Company(models.Model):
    locations = models.ManyToManyField(Location)
    currency = models.ForeignKey(Currency, null=True)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)

class Account(models.Model):
    company = models.ForeignKey(Company)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class Service(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class Contact(models.Model):
    name = models.CharField(max_length=100)
    account = models.ForeignKey(Account)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)

class Packages(models.Model):
    # RELACAO ENTRE TABELAS
    account = models.ForeignKey(Account)
    services = models.ManyToManyField(Service)
    locations = models.ManyToManyField(Location, through='PackagesPrices')
    cname_url_filter = models.ForeignKey(LocationLevel)

    # DADOS DO PACOTE
    default_unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    default_excess_unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    final_price = models.DecimalField(max_digits=20, decimal_places=4)


class PackagesPrices(models.Model):
    location = models.ForeignKey(Location)
    package = models.ForeignKey(Packages)
    amount = models.IntegerField()
    unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    excess_unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    final_price = models.DecimalField(max_digits=20, decimal_places=4)


class ProgressiveDiscounts(models.Model):
    # RELACAO ENTRE TABELAS
    service = models.ForeignKey(Service)
    location = models.ForeignKey(Location)
    company = models.ForeignKey(Company)

    # DADOS DO PROGRESSIVE DISCOUNTS
    amount = models.IntegerField()
    unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    excess_unit_cost = models.DecimalField(max_digits=20, decimal_places=4)
    final_price = models.DecimalField(max_digits=20, decimal_places=4)


class Configuration(models.Model):
    # RELACAO ENTRE TABELAS
    service = models.ForeignKey(Service)
    account = models.ForeignKey(Account)
    locations = models.ManyToManyField(Location)

    # DADOS DA CONFIGURACAO
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)


class CnameUrl(models.Model):
    # RELACAO ENTRE TABELAS
    configuration = models.ForeignKey(Configuration)

    # DADOS DO CNAMEURL
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return u'{name}'.format(name=self.name)