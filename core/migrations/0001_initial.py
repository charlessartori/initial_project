# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LocationLevel'
        db.create_table(u'core_locationlevel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('level', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'core', ['LocationLevel'])

        # Adding model 'Location'
        db.create_table(u'core_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.LocationLevel'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Location'])

        # Adding model 'Currency'
        db.create_table(u'core_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('symbol', self.gf('django.db.models.fields.CharField')(max_length=5)),
        ))
        db.send_create_signal(u'core', ['Currency'])

        # Adding model 'Company'
        db.create_table(u'core_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Currency'], null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Company'])

        # Adding M2M table for field locations on 'Company'
        db.create_table(u'core_company_locations', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('company', models.ForeignKey(orm[u'core.company'], null=False)),
            ('location', models.ForeignKey(orm[u'core.location'], null=False))
        ))
        db.create_unique(u'core_company_locations', ['company_id', 'location_id'])

        # Adding model 'Account'
        db.create_table(u'core_account', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Account'])

        # Adding model 'Service'
        db.create_table(u'core_service', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Service'])

        # Adding model 'Packages'
        db.create_table(u'core_packages', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('cname_url_filter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.LocationLevel'])),
            ('default_unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('default_excess_unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('final_price', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
        ))
        db.send_create_signal(u'core', ['Packages'])

        # Adding M2M table for field services on 'Packages'
        db.create_table(u'core_packages_services', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('packages', models.ForeignKey(orm[u'core.packages'], null=False)),
            ('service', models.ForeignKey(orm[u'core.service'], null=False))
        ))
        db.create_unique(u'core_packages_services', ['packages_id', 'service_id'])

        # Adding model 'PackagesPrices'
        db.create_table(u'core_packagesprices', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Location'])),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Packages'])),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('excess_unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('final_price', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
        ))
        db.send_create_signal(u'core', ['PackagesPrices'])

        # Adding model 'ProgressiveDiscounts'
        db.create_table(u'core_progressivediscounts', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('service', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Service'])),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Location'])),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Company'])),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('excess_unit_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
            ('final_price', self.gf('django.db.models.fields.DecimalField')(max_digits=20, decimal_places=4)),
        ))
        db.send_create_signal(u'core', ['ProgressiveDiscounts'])

        # Adding model 'Configuration'
        db.create_table(u'core_configuration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('service', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Service'])),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['Configuration'])

        # Adding M2M table for field locations on 'Configuration'
        db.create_table(u'core_configuration_locations', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('configuration', models.ForeignKey(orm[u'core.configuration'], null=False)),
            ('location', models.ForeignKey(orm[u'core.location'], null=False))
        ))
        db.create_unique(u'core_configuration_locations', ['configuration_id', 'location_id'])

        # Adding model 'CnameUrl'
        db.create_table(u'core_cnameurl', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('configuration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Configuration'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'core', ['CnameUrl'])


    def backwards(self, orm):
        # Deleting model 'LocationLevel'
        db.delete_table(u'core_locationlevel')

        # Deleting model 'Location'
        db.delete_table(u'core_location')

        # Deleting model 'Currency'
        db.delete_table(u'core_currency')

        # Deleting model 'Company'
        db.delete_table(u'core_company')

        # Removing M2M table for field locations on 'Company'
        db.delete_table('core_company_locations')

        # Deleting model 'Account'
        db.delete_table(u'core_account')

        # Deleting model 'Service'
        db.delete_table(u'core_service')

        # Deleting model 'Packages'
        db.delete_table(u'core_packages')

        # Removing M2M table for field services on 'Packages'
        db.delete_table('core_packages_services')

        # Deleting model 'PackagesPrices'
        db.delete_table(u'core_packagesprices')

        # Deleting model 'ProgressiveDiscounts'
        db.delete_table(u'core_progressivediscounts')

        # Deleting model 'Configuration'
        db.delete_table(u'core_configuration')

        # Removing M2M table for field locations on 'Configuration'
        db.delete_table('core_configuration_locations')

        # Deleting model 'CnameUrl'
        db.delete_table(u'core_cnameurl')


    models = {
        u'core.account': {
            'Meta': {'object_name': 'Account'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.cnameurl': {
            'Meta': {'object_name': 'CnameUrl'},
            'configuration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Configuration']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.company': {
            'Meta': {'object_name': 'Company'},
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Currency']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Location']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.configuration': {
            'Meta': {'object_name': 'Configuration'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Account']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Location']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Service']"})
        },
        u'core.currency': {
            'Meta': {'object_name': 'Currency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'symbol': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'core.location': {
            'Meta': {'object_name': 'Location'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_level': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocationLevel']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.locationlevel': {
            'Meta': {'object_name': 'LocationLevel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.packages': {
            'Meta': {'object_name': 'Packages'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Account']"}),
            'cname_url_filter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.LocationLevel']"}),
            'default_excess_unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            'default_unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            'final_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Location']", 'through': u"orm['core.PackagesPrices']", 'symmetrical': 'False'}),
            'services': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['core.Service']", 'symmetrical': 'False'})
        },
        u'core.packagesprices': {
            'Meta': {'object_name': 'PackagesPrices'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'excess_unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            'final_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Location']"}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Packages']"}),
            'unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'})
        },
        u'core.progressivediscounts': {
            'Meta': {'object_name': 'ProgressiveDiscounts'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Company']"}),
            'excess_unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            'final_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Location']"}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Service']"}),
            'unit_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '4'})
        },
        u'core.service': {
            'Meta': {'object_name': 'Service'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['core']