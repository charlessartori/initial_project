#coding: utf-8
from django.test import TestCase
from core.models import *
from model_mommy import mommy


class CoreTest(TestCase):
    def setUp(self):
        self.location_level_pais = mommy.make(LocationLevel, name='Pais', level='3')
        self.location_level_cidade = mommy.make(LocationLevel, name='Cidade', level='6')

        self.location_brasil = mommy.make(Location, name='Brasil', location_level=self.location_level_pais)
        self.location_eua = mommy.make(Location, name='EUA', location_level=self.location_level_pais)
        self.location_poa = mommy.make(Location, name='POA', location_level=self.location_level_cidade)

        self.company_brasil = mommy.make(Company, name='Azion Brasil', locations=[self.location_brasil, self.location_poa])
        self.company_inc = mommy.make(Company, name='Azion Inc', locations=[self.location_brasil, self.location_eua])

        self.service_hc = mommy.make(Service, name='HTTP Caching')
        self.service_ls = mommy.make(Service, name='Live Streaming')

        ###### Azion Brasil
        mommy.make(ProgressiveDiscounts,
                   amount=1,
                   unit_cost=1,
                   excess_unit_cost=1.2,
                   final_price=1,
                   location=self.location_brasil,
                   company=self.company_brasil,
                   service=self.service_hc)

        mommy.make(ProgressiveDiscounts,
                   amount=1,
                   unit_cost=1.5,
                   excess_unit_cost=2,
                   final_price=1.5,
                   location=self.location_poa,
                   company=self.company_brasil,
                   service=self.service_hc)

        ###### Azion Inc
        mommy.make(ProgressiveDiscounts,
                   amount=1,
                   unit_cost=0.5,
                   excess_unit_cost=1,
                   final_price=0.5,
                   location=self.location_eua,
                   company=self.company_inc,
                   service=self.service_hc)

        mommy.make(ProgressiveDiscounts,
                   amount=1,
                   unit_cost=0.5,
                   excess_unit_cost=1,
                   final_price=0.5,
                   location=self.location_brasil,
                   company=self.company_inc,
                   service=self.service_hc)

        ###### RBS
        self.account_rbs = mommy.make(Account, name='RBS', company=self.company_brasil)

        ###### SONY
        self.account_sony = mommy.make(Account, name='Sony', company=self.company_inc)

        ###### PACKAGES
        package = mommy.make(Packages,
                             account=self.account_rbs,
                             locations=[self.location_brasil, self.location_poa],
                             services=[self.service_hc, self.service_ls],
                             cname_url_filter=self.location_level_pais,
                             default_unit_cost=0.5,
                             default_excess_unit_cost=1,
                             final_price=250,)

        pack = PackagesPrices.objects.get(location=self.location_brasil)

        pack.location = self.location_brasil
        pack.package = package
        pack.amount = 100
        pack.unit_cost = 1
        pack.excess_unit_cost = 1.2
        pack.final_price = 100
        pack.save()

        pack = PackagesPrices.objects.get(location=self.location_poa)

        pack.location = self.location_poa
        pack.package = package
        pack.amount = 100
        pack.unit_cost = 1.5
        pack.excess_unit_cost = 2
        pack.final_price = 150
        pack.save()

    def test_prices_in_progressive_discounts_filterby_company_location_1(self):

        pd = ProgressiveDiscounts.objects.get(company__name='Azion Brasil',
                                              location__name='Brasil')
        self.assertEqual(1, pd.final_price)

    def test_location_avaiable_in_company_brasil_2(self):
        company = Company.objects.get(name='Azion Brasil')
        locations = Location.objects.filter(company__name=company.name)

        self.assertEqual(2, len(locations))

    def test_not_location_eua_avaiable_in_company_brasil_2(self):
        company = Company.objects.get(name='Azion Brasil')
        locations = Location.objects.filter(company__name=company.name)

        location_names = [location.name for location in locations]
        self.assertTrue('EUA' not in location_names)

    def test_price_packages_filterby_location(self):


        account = Account.objects.get(name='RBS')

        packages_prices = account.packages_set.get().packagesprices_set.all()

        self.assertEqual(2, len(packages_prices))
        self.assertEqual(100, packages_prices[0].final_price)
        self.assertEqual(150, packages_prices[1].final_price)

    def test_location_avaiable_in_packages_4(self):
        locations = Location.objects.filter(company__name='Azion Brasil', progressivediscounts__company__name='Azion Brasil')
        self.assertEqual(locations[0].name, 'Brasil')
        self.assertEqual(locations[1].name, 'POA')
        self.assertEqual(2, len(locations))

    def test_location_level_filterby_package_6_7_8(self):

        package = Packages.objects.get(account__name='RBS')

        locations = Location.objects.filter(company__name='Azion Brasil',
                                        packages__account__name='RBS',
                                        packages__services__name='HTTP Caching',
                                        location_level=package.cname_url_filter)

        self.assertEqual(locations[0].name, 'Brasil')
        self.assertEqual(1, len(locations))
