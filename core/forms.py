from django import forms
from .models import *
from django.forms.models import modelformset_factory, inlineformset_factory


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company


class PackageForm(forms.ModelForm):
    class Meta:
        model = Packages
        exclude = ['locations']


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        exclude = ['account']
ContactFormSet = modelformset_factory(Contact, form=ContactForm, can_delete=True, extra=1)


class PackagePriceForm(forms.ModelForm):
    class Meta:
        model = PackagesPrices
PackageFormSet = modelformset_factory(Packages, form=PackageForm, can_delete=True, extra=1)
