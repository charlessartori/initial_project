from django.contrib import admin
from .models import *


class ProgressiveDiscountsInline(admin.TabularInline):
    model = ProgressiveDiscounts
    extra = 0


class ServiceAdmin(admin.ModelAdmin):
    inlines = [ProgressiveDiscountsInline]


class PackagesPricesInline(admin.TabularInline):
    model = PackagesPrices
    extra = 0


class LocationAdmin(admin.ModelAdmin):
    inlines = [PackagesPricesInline]


class PackageInline(admin.TabularInline):
    model = Packages
    extra = 0


admin.site.register(Account)
admin.site.register(Company)
admin.site.register(Location, LocationAdmin)
admin.site.register(PackagesPrices)
admin.site.register(Packages)
admin.site.register(Configuration)
admin.site.register(CnameUrl)
admin.site.register(Service, ServiceAdmin)
admin.site.register(ProgressiveDiscounts)
admin.site.register(LocationLevel)
admin.site.register(Currency)
admin.site.register(Contact)

