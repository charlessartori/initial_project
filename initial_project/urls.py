from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from core.forms import *
from core.views import *


urlpatterns = patterns('',
    url(r'^$', 'core.views.homepage', name='homepage'),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^account/create/$', 'core.views.account_create', name='account_create'),
    url(r'^account/edit/$', 'core.views.account_edit', name='account_edit'),
    url(r'^package/edit/(?P<package_id>\d+)/$', 'core.views.package_edit', name='package_edit'),
    url(r'^package/create/$', 'core.views.package_create', name='package_create'),

)
